(function(){
  'use strict';
  var module = angular.module('feeding-tracker', ['onsen']);

  module.controller('DetailController', function($scope, $data) {
    $scope.item = $data.selectedItem;
  })

  module.controller('FeedingController', function($scope, $data) {
    $scope.item = $data.selectedItem;
  })

  module.controller('MasterController', function($scope, $data) {
    $scope.items = $data.items;

    $scope.showDetail = function(index) {
      var selectedItem = $data.items[index];
      $data.selectedItem = selectedItem;
      $scope.ons.navigator.pushPage('detail.html', {title : selectedItem.title});
    }

    $scope.startFeeding = function(index) {
      $scope.ons.navigator.pushPage('feeding.html', {title : 'New Feeding'});
    }
  });

  module.factory('$data', function() {
      var data = {};
      data.items = [
          {
              title: 'Feeding #1',
              icon: 'dot-circle-o',
              description: 'Feeding was @ 11:00 PM and lasted for a total of 15 min',
              type: 'Breast Fed'
          },
          {
              title: 'Feeding #2',
              icon: 'dot-circle',
              description: 'Feeding was @ 5:00 AM and lasted for a total of 20 min',
              type: 'Bottle Fed'
          },
          {
              title: 'Feeding #3',
              icon: 'dot-circle-o',
              description: 'Feeding was @ 8:00 AM and lasted for a total of 15 min',
              type: 'Breast Fed'
          }
      ];
      return data;
  });
})();

